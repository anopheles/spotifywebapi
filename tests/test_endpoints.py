import unittest
from unittest import TestCase

from spotifywebapi.api import query_endpoint, EndPoint
from spotifywebapi.entities import FullArtist, SimplifiedArtist, FullTrack, SimplifiedTrack, SimplifiedAlbum


class TestEndpoints(TestCase):

    def test_endpoint_search_album(self):
        response = query_endpoint(EndPoint.SEARCH_ALBUM, dict(q="Keeper of the seven"))
        albums = list(response.extract_albums())
        self.assertTrue(any("Keeper of the Seven Keys" in album.name for album in albums))

    def test_endpoint_search_track(self):
        response = query_endpoint(EndPoint.SEARCH_TRACK, dict(q="Reach out for the light"))
        tracks = list(response.extract_tracks())
        track_name = "Reach Out For The Light (Recorded @ Wacken & Masters of Rock)"
        self.assertTrue(any(track_name in track.name for track in tracks))

    def test_endpoint_search_track_with_market(self):
        response = query_endpoint(EndPoint.SEARCH_TRACK, dict(q="Reach out for the light", market="DE"))
        tracks = list(response.extract_tracks())
        track_name = "Reach out for the Light"
        self.assertTrue(any(track_name in track.name for track in tracks))
        track_name_not_present = "Reach Out For The Light (Recorded @ Wacken & Masters of Rock)"
        self.assertFalse(any(track_name_not_present in track.name for track in tracks))

    def test_endpoint_search_artist(self):
        response = query_endpoint(EndPoint.SEARCH_ARTIST, dict(q="Michael Kiske"))
        artists = list(response.extract_artists())
        self.assertTrue(any("Michael Kiske & Amanda Somerville" in artists.name for artists in artists))

    def test_endpoint_search_extract_all_items(self):
        response = query_endpoint(EndPoint.SEARCH_ARTIST, dict(q="Royal Hunt"))
        items = list(response.extract_items())
        self.assertTrue(any("Royal Hunt" in item.name for item in items))

    def test_track(self):
        response = query_endpoint(EndPoint.TRACK, dict(id="1YrC8s6yZWw23QxW6rfM9f"))
        tracks = list(response.extract_tracks())
        self.assertEqual(len(tracks), 1)
        self.assertTrue(any("Come" in track.name for track in tracks))

    def test_album_tracks(self):
        response = query_endpoint(EndPoint.ALBUM_TRACKS, dict(id="0wmVzhQ5ogGhDeejznT4bz"))
        tracks = list(response.extract_tracks())
        self.assertTrue(any("Hunting High And Low" in track.name for track in tracks))
        self.assertTrue(all(isinstance(track, FullTrack) for track in tracks))

    def test_albums(self):
        response = query_endpoint(EndPoint.ALBUMS, dict(id="0wmVzhQ5ogGhDeejznT4bz"))
        tracks = list(response.extract_tracks())
        self.assertTrue(any("Hunting High And Low" in track.name for track in tracks))
        self.assertTrue(any("Neon Light Child" in track.name for track in tracks))
        self.assertTrue(all(isinstance(track, SimplifiedTrack) for track in tracks))

    def test_artists(self):
        response = query_endpoint(EndPoint.ARTISTS, dict(id="0YGfQfOPRI89rtSReS9J4H"))
        artists = list(response.extract_artists())
        self.assertTrue(any("Michael Kiske & Amanda Somerville" in artists.name for artists in artists))
        self.assertTrue(all(isinstance(artist, FullArtist) for artist in artists))

    def test_artists_album(self):
        response = query_endpoint(EndPoint.ARTISTS_ALBUM, dict(id="3TOqt5oJwL9BE2NG9MEwDa"))
        albums = list(response.extract_albums())
        self.assertTrue(any("Ten Thousand Fists" in album.name for album in albums))
        self.assertTrue(all(isinstance(artist, SimplifiedAlbum) for artist in albums))

    def test_artists_album_with_many_albums(self):
        response = query_endpoint(EndPoint.ARTISTS_ALBUM, dict(id="6mdiAmATAx73kdxrNrnlao"))
        albums = list(response.extract_albums(extract_pages=True))
        self.assertGreaterEqual(len(albums), 62)

    def test_artists_album_with_many_albums_without_page_extraction(self):
        response = query_endpoint(EndPoint.ARTISTS_ALBUM, dict(id="6mdiAmATAx73kdxrNrnlao"))
        albums = list(response.extract_albums(extract_pages=False))
        self.assertLessEqual(len(albums), 50)

    def test_album_tracks_with_paging(self):
        album_id = "2rA6ZfJR23Bg7oX9sWzTUf"
        response = query_endpoint(EndPoint.ALBUM_TRACKS, dict(id=album_id, limit=50))
        tracks = list(response.extract_tracks(extract_pages=True))
        self.assertGreater(len(tracks), 50)
        self.assertTrue(all(isinstance(tracks, FullTrack) for tracks in tracks))
        self.assertTrue(any("herzliebster" in track.name.lower() for track in tracks))
        self.assertTrue(any("meiner wangen" in track.name.lower() for track in tracks))
        self.assertGreater(len(set(track.name for track in tracks)), 50)

    def test_albums_with_paging(self):
        album_id = "2rA6ZfJR23Bg7oX9sWzTUf"
        response = query_endpoint(EndPoint.ALBUMS, dict(id=album_id, limit=50))
        tracks = list(response.extract_tracks(extract_pages=True))
        self.assertTrue(all(isinstance(tracks, SimplifiedTrack) for tracks in tracks))
        self.assertTrue(any("meiner wangen" in track.name.lower() for track in tracks))

    def test_is_okay(self):
        response = query_endpoint(EndPoint.ALBUMS, dict(id="0wmVzhQ5ogGhDeejznT4bz"))
        self.assertTrue(response.is_okay())

    def test_albums_with_artists(self):
        album_id = "2rA6ZfJR23Bg7oX9sWzTUf"
        response = query_endpoint(EndPoint.ALBUMS, dict(id=album_id, limit=50))
        artists = list(response.extract_artists())
        self.assertTrue(all(isinstance(artist, SimplifiedArtist) for artist in artists))
        self.assertTrue(any("Johann Sebastian Bach" in artists.name for artists in artists))

    def test_title_of_albums_of_artist(self):
        response = query_endpoint(EndPoint.ARTISTS_ALBUM, dict(id="4pQN0GB0fNEEOfQCaWotsY"))
        albums = list(response.extract_albums())
        self.assertTrue(all(isinstance(album, SimplifiedAlbum) for album in albums))
        self.assertTrue(any("Ride the Sky: The Very Best of 1985-1998" in album.name for album in albums))

    @unittest.skip("auth token required")
    def test_playlist_tracks(self):
        response = query_endpoint(EndPoint.PLAYLIST_TRACKS, dict(playlist_id='4pTcidpQIJN04nyv2CWluw', user_id="prinzschleifer"), auth_token=None)
        tracks = list(response.extract_tracks())
        self.assertTrue(all(isinstance(album, FullTrack) for album in tracks))
        for track in tracks:
            print(track)

