#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from unittest import TestCase

import sys

from spotifywebapi import api

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logging.getLogger(__name__)


class TestAPI(TestCase):

    def test_offset_generation(self):
        self.assertEqual(api.get_offsets(20, 24), [20])

    def test_offset_generation_simple(self):
        self.assertEqual(api.get_offsets(20, 80), [20, 40, 60])
