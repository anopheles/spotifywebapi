#!/usr/bin/env python

from distutils.core import setup

from pip.req import parse_requirements

install_reqs = parse_requirements('requirements.txt', session='hack')

setup(name ='spotifywebapi',
      version='0.0.1',
      packages=['spotifywebapi'],
      install_requires=[str(ir.req) for ir in install_reqs]
)