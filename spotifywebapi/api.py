#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import enum
import functools
import itertools
import logging
import math
import urllib.parse

import requests
from requests import Response
from requests_futures.sessions import FuturesSession

from spotifywebapi.utils import extract_single_json_response
from spotifywebapi.entities import FullAlbum, FullArtist, SimplifiedArtist, SimplifiedPlayList, FullTrack, \
    SimplifiedTrack, SimplifiedAlbum

BASE_URL = "https://api.spotify.com"

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class EndPoint(enum.Enum):
    """
    Implementation of the Web API Endpoints defined by
        https://developer.spotify.com/web-api/endpoint-reference/
    """
    SEARCH_ALBUM = ("/v1/search", dict(type="album"), "Search for Album")
    SEARCH_PLAYLIST = ("/v1/search", dict(type="playlist"), "Search for Playlist")
    SEARCH_TRACK = ("/v1/search", dict(type="track"), "Search for Track")
    SEARCH_ARTIST = ("/v1/search", dict(type="artist"), "Search for Artist")

    TRACK = ('v1/tracks/{id}', dict())

    ALBUM_TRACKS = ("/v1/albums/{id}/tracks", dict())
    ALBUMS = ("/v1/albums/{id}", dict())

    ARTISTS = ("/v1/artists/{id}", dict())
    ARTISTS_ALBUM = ("/v1/artists/{id}/albums", dict(limit=50))

    ME = ("v1/me", dict())
    TOP_ME = ("v1/me/top/{type}", dict())
    PLAYLIST_CURRENT_USER = ("v1/me/playlists", dict())
    TRACKS_CURRENT_USER = ("v1/me/tracks", dict())
    PLAYLIST_TRACKS = ("v1/users/{user_id}/playlists/{playlist_id}/tracks", dict())

    PLAYLIST_ADD = ("v1/users/{user_id}/playlists", dict(), "", requests.post)
    PLAYLIST_TRACK_ADD = ("v1/users/{user_id}/playlists/{playlist_id}/tracks", dict(), "", requests.post)

    PLAY = ("v1/me/player/play", dict(), "Start a new context or resume current playback on the userâ€™s active device.", requests.put)
    PLAYER = ("v1/me/player", dict(), "Get information about the userâ€™s current playback state, including track, track progress, and active device.", requests.get)

    RECOMMENDATIONS = ("v1/recommendations", dict(), "Create a playlist-style listening experience based on seed artists, tracks and genres.", requests.get)

    def __init__(self, path: str, query: dict, description="", method=requests.get):
        self.path = path
        self.query = query
        self.description = description
        self.method = method


SEARCH_ENDPOINTS = (EndPoint.SEARCH_ALBUM, EndPoint.SEARCH_ARTIST, EndPoint.SEARCH_TRACK, EndPoint.SEARCH_PLAYLIST)
ALBUM_ENDPOINTS = (EndPoint.ALBUM_TRACKS, EndPoint.ALBUMS)
ARTIST_ENDPOINTS = (EndPoint.ARTISTS, EndPoint.ARTISTS_ALBUM)


class SpotifyException(Exception):
    pass


class SpotifyAPIResponse:
    def __init__(self, endpoint: EndPoint, main_response: Response, query: dict, url=""):
        self.endpoint = endpoint
        self.main_response = main_response
        self.date = datetime.datetime.now()
        self.url = url
        self.query = query

    @staticmethod
    def extract_paging_object(endpoint, json_response):
        paging_object = json_response
        if endpoint in (EndPoint.ALBUMS, ):
            paging_object = json_response["tracks"]
        try:
            return paging_object["limit"], paging_object["total"],  paging_object["next"]
        except KeyError:
            return None, None, None

    @staticmethod
    def extract_pages(endpoint, json_response, headers=None):
        limit, total, next_url = SpotifyAPIResponse.extract_paging_object(endpoint, json_response)
        logger.debug("extracting page with limit: {}, total: {}, next_url:{}".format(limit, total, next_url))
        if not next_url:
            logger.debug("there is no next page")
            return []

        paged_endpoint = endpoint
        if endpoint == EndPoint.ALBUMS:
            paged_endpoint = EndPoint.ALBUM_TRACKS

        session = FuturesSession()
        page_responses = []
        for offset in get_offsets(limit, total):
            logging_template = "extracting page: offset={}, limit={}, total={}, next_url={}"
            logger.debug(logging_template.format(offset, limit, total, next_url))

            # we can't use self.url to calculate the next page,
            # since the current endpoint might reference a totally different one, e.g. ALBUMS -> ALBUM_TRACKS)
            # next_url can't be used either, since only the next page can be accessed, but WE WANT THEM ALL
            query_parameters = dict()
            query_parameters["offset"] = offset
            query_parameters["limit"] = limit
            logger.debug("query parameter {}".format(query_parameters))

            # TODO replace with urlparse
            query_url = next_url[:next_url.find('?')]
            logger.debug("url {}".format(query_url))
            future_response = session.get(query_url, params=query_parameters, headers=headers)
            page_responses.append((paged_endpoint, future_response))

        return page_responses

    def get_album_id(self):
        if self.endpoint in ALBUM_ENDPOINTS:
            return self.query["id"]

    def is_too_old(self):
        seconds = int(self.main_response.headers["Cache-Control"].split("=")[-1])
        return (datetime.datetime.now() - self.date).total_seconds() > seconds

    @property
    def json_response(self):
        return extract_single_json_response(self.main_response)

    def extract_entity(self, entity_class, entity_name=None, **kwargs):
        try:
            items = self.extract_raw_items(entity_name, **kwargs)
            logger.debug("{} responses extracted containing {} individual items"
                         .format(len(items), len(list(itertools.chain(*items)))))
            return list(map(functools.partial(entity_class, parent_response=self.json_response), itertools.chain(*items)))
        except (KeyError, TypeError):
            logger.error("error extracting items", exc_info=True)
            logger.error("endpoint: {}".format(self.endpoint))
            logger.error("response: {}".format(self.json_response))
            return []

    def extract_raw_items(self, entity_name, extract_pages=False):
        items = []
        responses_from_pages = SpotifyAPIResponse.extract_pages(self.endpoint, self.json_response, headers=self.main_response.request.headers) if extract_pages else []
        for endpoint, response in [(self.endpoint, self.main_response)] + responses_from_pages:
            json_response = extract_single_json_response(response)
            logger.debug("trying to extract entity {} and endpoint {} with {}".format(entity_name, self.endpoint, json_response.keys()))
            if endpoint in SEARCH_ENDPOINTS + (EndPoint.ALBUMS, ):
                try:
                    items.append(json_response[entity_name]["items"])
                except KeyError:
                    logger.debug("tried to extract {} but only {} available".format(entity_name, json_response.keys()))
                except TypeError:
                    # json_response[entity_name] is not a paging object
                    items.append(json_response[entity_name])

            elif endpoint in (EndPoint.TRACK, EndPoint.ARTISTS):
                items.append([json_response])
            elif endpoint in (EndPoint.ARTISTS_ALBUM, EndPoint.ALBUM_TRACKS, EndPoint.TOP_ME):
                items.append(json_response["items"])
            elif endpoint in (EndPoint.PLAYLIST_CURRENT_USER, ):
                items.append(json_response["items"])
            elif endpoint in (EndPoint.PLAYLIST_TRACKS, EndPoint.TRACKS_CURRENT_USER):
                items.append([playlist_track["track"] for playlist_track in json_response["items"]])
            elif endpoint in (EndPoint.ME, ):
                items.append(json_response)
            else:
                logger.error("this should not happen, since all cases have been considered, or did I miss one?")
        return items

    def extract_playlists(self, **kwargs):
        return self.extract_entity(SimplifiedPlayList, entity_name="playlists", **kwargs)

    def extract_artists(self, **kwargs):
        if self.endpoint in (EndPoint.ALBUMS, ):
            return self.extract_entity(SimplifiedArtist, entity_name="artists", **kwargs)
        return self.extract_entity(FullArtist, entity_name="artists", **kwargs)

    def extract_tracks(self, **kwargs):
        if self.endpoint in (EndPoint.ALBUMS, ):
            return self.extract_entity(SimplifiedTrack, entity_name="tracks", **kwargs)
        else:
            return self.extract_entity(FullTrack, entity_name="tracks", **kwargs)

    def extract_albums(self, **kwargs):
        if self.endpoint in (EndPoint.ARTISTS_ALBUM, ):
            return self.extract_entity(SimplifiedAlbum, entity_name="albums", **kwargs)
        return self.extract_entity(FullAlbum, entity_name="albums", **kwargs)

    def extract_items(self, **kwargs):
        return itertools.chain(self.extract_playlists(**kwargs),
                               self.extract_tracks(**kwargs),
                               self.extract_albums(**kwargs),
                               self.extract_artists(**kwargs))

    def is_okay(self):
        return all(response.status_code == requests.codes.ok for response in [self.main_response])

    def get_name(self):
        if self.endpoint in SEARCH_ENDPOINTS:
            return "Search: {query} ({type})".format(query=self.query["q"], type=self.query["type"])
        elif self.endpoint in ALBUM_ENDPOINTS:
            return self.json_response["name"]
        elif self.endpoint in ARTIST_ENDPOINTS:
            # todo get artist name
            return "Albums"
        elif self.endpoint in (EndPoint.PLAYLIST_TRACKS, ):
            return "Playlist"
        return ""

    def __repr__(self):
        return "<SpotifyAPIResponse({},{})>".format(self.endpoint, self.query)


def get_offsets(limit, total):
    return [limit * x for x in range(1, math.ceil(total / limit))]


def query_endpoint(endpoint: EndPoint, query_parameters: dict = None,
                   headers=None, auth_token=None, data=None) -> SpotifyAPIResponse:
    if not query_parameters:
        query_parameters = dict()
    query_parameters.update(endpoint.query)
    url = urllib.parse.urljoin(BASE_URL, endpoint.path.format(**query_parameters))
    logging.debug("queried_url {} with parameters {}".format(url, query_parameters))

    if not headers:
        headers = dict()
    if auth_token:
        headers["Authorization"] = "Bearer {}".format(auth_token)

    return SpotifyAPIResponse(endpoint,
                              endpoint.method(url, params=query_parameters, headers=headers, data=data),
                              query_parameters,
                              url)


def main():
    pass


if __name__ == '__main__':
    main()
