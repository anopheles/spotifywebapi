#!/usr/bin/env python
# -*- coding: utf-8 -*-


class SpotifyEntity:
    def __init__(self, spotify_object, parent_response=None):
        self.spotify_object = spotify_object
        self.parent_response = parent_response

    def __getattr__(self, item):
        return self.spotify_object[item]

    def __repr__(self):
        try:
            return '{}("{}", id={})'.format(self.__class__.__name__, self.name, self.id)
        except TypeError:
            return super(SpotifyEntity, self).__repr__()

    @property
    def title(self):
        return self.name


class FullAlbum(SpotifyEntity):
    @property
    def album_id(self):
        return self.spotify_object["id"]

    @property
    def album_images(self):
        return self.spotify_object["images"]


class FullArtist(SpotifyEntity):
    @property
    def album_id(self):
        return self.spotify_object["id"]

    @property
    def album_images(self):
        return self.spotify_object["images"]


class SimplifiedAlbum(SpotifyEntity):
    pass


class SimplifiedArtist(SpotifyEntity):
    pass


class SimplifiedPlayList(SpotifyEntity):
    pass


class Track(SpotifyEntity):

    @property
    def artists(self):
        return [SimplifiedArtist(simplified_artist, self.parent_response) for simplified_artist in
                self.spotify_object["artists"]]

    @property
    def title(self):
        return " - ".join([self.name, self.album.name, ", ".join([artist.name for artist in self.artists])])


class FullTrack(Track):

    @property
    def album(self):
        return SimplifiedAlbum(self.spotify_object["album"], self.parent_response)

    @property
    def album_id(self):
        return self.album.id

    @property
    def album_images(self):
        return self.album.images


class SimplifiedTrack(Track):

    @property
    def album(self):
        return SimplifiedAlbum(self.parent_response)

    @property
    def album_id(self):
        return self.parent_response["id"]

    @property
    def album_images(self):
        return self.parent_response["images"]