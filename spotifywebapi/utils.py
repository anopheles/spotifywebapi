#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

from requests import Response

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def extract_single_json_response(response: Response):
    """
        Handles normal responses and future responses.
    """
    try:
        return response.json()
    except AttributeError:
        return response.result().json()